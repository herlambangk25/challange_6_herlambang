# RESTful API Authentication Challange 6


## link postmen 

```
https://documenter.getpostman.com/view/7568322/2s8Yeixbbo
```

## link Record 

```
https://youtu.be/Zaq8ykqh5wo
```

Challange kali ini menggabungkan antara chapter 3 dan 4 dengan `login`.

versi sekarang {versi} = v1

Endpoint ini menggunakan prefix `/api/{versi}`.
=> /api/v1/

Di challenge ini, yang bakal kamu
lakuin adalah:

1. Pindahkan code challenge pada chapter 3 dan 4
   yang semula merupakan HTML statis ke dalam
   server menggunakan Express, halaman 1
   dengan yang lainnya dibedakan dengan routing
2. Membuat data user static untuk login di bagian
   backend
3. Menggunakan Postman untuk mengecek API
4. Serving data user static ke bentuk JSON

Mari kita breakdown fitur - fitur yang dimiliki oleh sistem ini.

jangan lupa instal npm

- `POST` /auth/login

Payload

```json
{
  "username": "email@address.com",
  "password": "secretPassword"
}
```

Response

```json
{
  "accessToken": "secretToken",
  "accessTokenExpired": "2022/02/01T22:30:10.000Z",
  "refreshToken": "refreshToken",
  "refreshTokenExpired": "2022/02/01T22:30:10.000Z"
}
```

- `GET` /me

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
{
  ...userData
}
```

- `GET` /users

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
[
  { ...userData },
  { ...userData }
]
```

## Credentials

Username: `porsche.huel@email.com`
Password: `password123`

Username: `julius.oconner@email.com`
Password: `password123456`

Username: `melodee.dooley@email.com`
Password: `password123456789`


## MODEL
```
LOGIN

npx sequelize-cli model:generate --name User_game --attributes username:string,password:string
```

```
USER_GAME_HISTORY

npx sequelize-cli model:generate --name User_game_history --attributes game_name:string,time_playing:date
```

```
USER_GAME_BIODATA

npx sequelize-cli model:generate --name User_game_biodata --attributes name:string username:string,game_played:string,total_playing:date
```


## GET GAME BIO <http://localhost:8000/api/v1/auth/game-bio/3>

```
{
    "id": 3,
    "name": "steff",
    "id_user_game": 3,
    "total_playing": "2022-11-07T17:34:02.095Z",
    "createdAt": "2022-11-08T16:52:09.634Z",
    "updatedAt": "2022-11-08T16:52:09.634Z",
    "User_game_histories": [
        {
            "id": 1,
            "game_name": "Valorant",
            "time_playing": "2022-11-07T17:34:02.095Z",
            "createdAt": "2022-11-08T16:56:20.482Z",
            "updatedAt": "2022-11-08T16:56:20.482Z",
            "id_user_game": 3
        },
        {
            "id": 2,
            "game_name": "OverCook",
            "time_playing": "2022-11-07T17:34:02.095Z",
            "createdAt": "2022-11-08T17:01:16.688Z",
            "updatedAt": "2022-11-08T17:01:16.688Z",
            "id_user_game": 3
        },
        {
            "id": 3,
            "game_name": "HarvestMoon",
            "time_playing": "2022-11-07T17:34:02.095Z",
            "createdAt": "2022-11-08T17:01:26.940Z",
            "updatedAt": "2022-11-08T17:01:26.940Z",
            "id_user_game": 3
        },
        {
            "id": 5,
            "game_name": "OverCook",
            "time_playing": "2022-11-07T17:34:02.095Z",
            "createdAt": "2022-11-08T17:17:57.256Z",
            "updatedAt": "2022-11-08T17:17:57.256Z",
            "id_user_game": 3
        }
    ]
}
```