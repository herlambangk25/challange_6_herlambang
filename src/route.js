const express = require("express");
const bcrypt = require("bcrypt");
// const { addHours, addMinutes } = require("date-fns");

// const db = require("./db/users.json");
// const { isAuthenticated, isLogin } = require("./middleware");
// const { generateToken } = require("./utils/common");
const route = express.Router();


const {User_game,User_game_biodata,User_game_history} = require("./services/models");

route.post(`/auth/register`, async(req,res)=> {
    try {
      const {username, password} = req.body;

      const userName = await User_game.findOne({
        where: {
          username: req.body.username,
        },
      });

        if (userName) {
        return res.json("username already taken");
      }
      
      //  console.log(req.body);
      const data = {
                username,
                password: await bcrypt.hash(password, 12)
            };
        const user_game = await User_game.create(data);
        res.json(user_game);
    } catch (error) {
          return res.json(error);
    }
});

route.post('/login/auth', async(req,res,next)=>{  
  var username = req.body.username  
  var password = req.body.password  
  const user = await User_game.findOne({
    where: {
      username,
      password
    }
  });
  
  if(!user){
    res.status(402).json({
        success: false
    })
  }else{
    res.status(200).json({
        success: true
    })
 }
});

route.get("/auth/user", async(req,res)=>{
    const user = await User_game.findAll();
    res.json(user);
});

route.put("/auth/user/:id", async(req,res)=>{
  // const user = await User_game.findAll();
  const {body} = req;
    const u_user = await User_game.update(body,{
        where:{
            id:req.params.id
        }
    });
    res.json(u_user);
})

route.delete("/auth/user/:id", async(req,res)=>{
    const u_user = await User_game.destroy({
          where:{
            id:req.params.id
        }
    });
    res.json(u_user);
})

//Game - Biodata

route.post(`/auth/game-bio`, async(req,res)=> {
   const {body} = req;
   console.log(req.body);
  //  console.log(req.body);
    const user_game = await User_game_biodata.create(body);
    res.json(user_game);
});

route.get("/auth/game-bio", async(req,res)=>{
    const user_game = await User_game_biodata.findAll(
        {
          include:[
            {
                model:User_game,
                attributes:["username"]
            }
        ]
      }
    );
    res.json(user_game);
});

route.get("/auth/game-bio/:id", async(req,res)=>{
    const user_game = await User_game_biodata.findOne(
      {
        where:{
            id:req.params.id
        },
          include:[
               {
                model:User_game_history
            }
        ]
      }
    );
    res.json(user_game);
})


//game_hitories 
route.post(`/auth/game-his`, async(req,res)=> {
   const {body} = req;
   console.log(req.body);
  //  console.log(req.body);
    const user_game = await User_game_history.create(body);
    res.json(user_game);
});


route.get("/auth/game-his/:id", async(req,res)=>{
    const user_game = await User_game_history.findOne(
      {
        where:{
            id:req.params.id
        },
          include:[
            {
                model:User_game_biodata
                                
                
            }
        ]
      }
    );
    res.json(user_game);
})



module.exports = route;
