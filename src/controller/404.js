// controller untuk render page game.ejs
function notFoundPage(req, res) {
  res.status(200);
  return res.render("404.ejs");
}

// export controller
module.exports = notFoundPage;
