// controller untuk render page index.ejs
function loginPage(req, res) {
  res.status(200);
  return res.render("login.ejs");
}

// export controller
module.exports = loginPage;
