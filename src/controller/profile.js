// controller untuk render page index.ejs
function profilePage(req, res) {
  res.status(200);
  return res.render("profile.ejs");
}

// export controller
module.exports = profilePage;
