'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      User_game_history.belongsTo(models.User_game_biodata,{
        foreignKey:'id_user_game'
      });

      // User_game_history.belongsTo(models.User_game_history,{
      //   foreignKey:'id_user_game'
      // });
      
    }
  }
  User_game_history.init({
    game_name: DataTypes.STRING,
    time_playing: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User_game_history',
  });
  return User_game_history;
};