'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //ini ke user game
      User_game_biodata.belongsTo(models.User_game,{
        foreignKey:'id_user_game'
      });

      User_game_biodata.hasMany(models.User_game_history,{
        foreignKey:'id_user_game'
      });

    }
  }
  User_game_biodata.init({
    name: DataTypes.STRING,
    id_user_game: DataTypes.INTEGER,
    total_playing: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User_game_biodata',
  });
  return User_game_biodata;
};