const jwt = require("jsonwebtoken");
const { addHours, isAfter } = require("date-fns");
// const { isLogin } = require("./verify");

/**
 * Access token checker flow
 * - Get access token from request headers
 * - Verifikasi access token menggunakan module jsonwebtoken
 * - Is token still valid?
 * - Valid ? go next
 */
const isAuthenticated = (req, res, next) => {
  console.log("auth:", req.headers.authorization);
  const accessToken = req.headers.authorization;

  if (!accessToken) {
    console.log("invalid token");
    res.render("404.ejs");
    return res.status(404).json({ error: "invalid token" });
    // return res.redirect("/login");
    // return res.status(404).json({ error: "Page not found" });
  }

  try {
    const isVerified = jwt.verify(accessToken, process.env.JWT_SECRET);
    if (!isVerified) {
      // console.log("not verived");
      // return res.redirect("/login");
      return res.status(404).json({ error: "jwt error" });
    }

    req.session = jwt.decode(accessToken);

    // Check expired token
    const currentHour = Date.now();
    const accessTokenExpiredAt = addHours(req.session.loggedInAt, 1);
    if (isAfter(currentHour, accessTokenExpiredAt)) {
      return res.status(404).json({ error: "Access token has expired." });
      // return res.redirect("/login");
    }

    next();
  } catch (error) {
    console.log("error", error);
    req.log.error(error);
    return res.status(404).json({ error: "Page not found" });
  }
};

const isLogin = (req, res, next) => {
  try {
    const accessToken = req.headers.authorization;
    console.log("accessToken :", accessToken);
  } catch (error) {
    console.log("error", error);
    req.log.error(error);
    return res.status(404).json({ error: "Page not found" });
  }
};

const logout = (req, res) => {
  // Hapus sesi user dari broser
  req.session.destroy((err) => {
    if (err) {
      return console.log(err);
    }
    // Hapus cokie yang masih tertinggal
    res.clearCookie("secretname");
    // res.redirect("/login");
  });
};

module.exports = { isAuthenticated, logout, isLogin };
