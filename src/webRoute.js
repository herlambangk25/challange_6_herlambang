const express = require("express");
const webRoute = express.Router();
const bcrypt = require("bcrypt");
var salt = bcrypt.genSaltSync(10);



const {User_game,User_game_biodata,User_game_history} = require("./services/models");

webRoute.get("/", (req,res)=>{
    res.render("../views/index.ejs")
});
webRoute.get("/game", (req,res)=>{
    res.render("../views/game.ejs")
});

webRoute.get("/login", (req,res)=>{
    res.render("../views/login.ejs",{message:null})
});

webRoute.get("/register", (req,res)=>{
    res.render("../views/register.ejs",{error:null})
});


webRoute.post("/register", async(req,res)=>{
   try {
      const {username, password} = req.body;
      const userName = await User_game.findOne({
        where: {
          username: req.body.username,
        },
      });
     if (userName) {
            res.render('../views/register.ejs',{
                 error:"Username already taken"
              });
         }
      //  console.log(req.body);
      const data = {
                username,
                password: await bcrypt.hash(password, 12)
            };
        const user_game = await User_game.create(data);
        res.render('../views/index',{
            error:null
        })

    } catch (error) {
          return res.json(error);
    }
});

webRoute.get("/me", async(req,res)=>{
    const data = await User_game.findAll();
    console.log(data);
     res.render('../views/profile.ejs',{
        data,
        message:null
    });

    // res.json(data)
});

webRoute.get("/playing", async(req,res)=>{
    const data = await User_game_biodata.findAll(
        {
          include:[
            {
                model:User_game,
                attributes:["username"]
            }
        ]
      }
    );
    res.render("../views/playing.ejs",{
        data
    })
});

webRoute.get("/playing-detail/:id", async(req,res)=>{
const data = await User_game_biodata.findOne(
      {
        where:{
            id:req.params.id
        },
          include:[
               {
                model:User_game_history
            }
        ]
      }
    );
    // res.json(data);

     res.render('../views/playing-detail.ejs',{
        data
     })
});

webRoute.post('/login/auth', async(req,res,next)=>{  
  var username = req.body.username  
  var password = req.body.password  
  const user = await User_game.findOne({
    where: {
      username,
      password
    }
  });

  if(!user){
     res.render('../views/login.ejs',{
                 message:"Maaf anda belom login"
              });
  }else{
     res.render('../views/login.ejs',{
                user,
                 message:`Selamat Datang ${ user.username}`
              });
 }
});

webRoute.get("/login/update/:id", async(req,res)=>{
    const data = await User_game.findOne({
        where:{
            id:req.params.id
        }
    });
     res.render('../views/update.ejs',{
        data,
        message:null
     })
});


webRoute.post('/login/update/:id', async(req,res,next)=>{  
  var username = req.body.username  
  var password = req.body.password  
   await User_game.update({
         ...req.body,
         password: await bcrypt.hash(password, 12)
    },{
    where: {
      id:req.params.id
    }
  });

 
     res.render('../views/login.ejs',{
                
                 message:`Berhasil di update `
              });
});


webRoute.get("/user/delete/:id", async(req,res)=>{
   await User_game.destroy({
        where:{
            id:req.params.id
        }
    });
    res.redirect("/me")
});

module.exports = webRoute;
