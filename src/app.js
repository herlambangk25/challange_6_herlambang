const express = require("express");
// const logger = require("pino-http");
// const dotenv = require("dotenv");

const route = require("./route");
const webRoute = require("./webRoute");

const server = (app) => {
  // dotenv.config();

  // Middleware logging
  // app.use(logger());

  // Middleware JSON dan Payload handler
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // app.use("/api/v1", route);
  // app.use("/", route);
  app.use(webRoute);
  app.use("/api/v1",route);

  // applying static middlewares
  app.use(express.static("public"));

  // applying ejs view engine
  app.set("view engine", "ejs");

  return app;
};

module.exports = server;
