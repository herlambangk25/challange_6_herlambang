class Start {
  constructor() {
    this.playerName = "Player";
    this.botName = "Deabot";
    this.playerOption;
    this.botOption;
    this.winner = "";
  }

  get getBotOption() {
    return this.botOption;
  }

  set setBotOption(option) {
    this.botOption = option;
  }

  get getPlayerOption() {
    return this.playerOption;
  }

  set setPlayerOption(option) {
    this.playerOption = option;
  }

  botBrain() {
    const option = ["paper", "scissor", "rock"];
    const bot = option[Math.floor(Math.random() * option.length)];
    return bot;
  }

  winCalculation() {
    if (this.botOption == "paper" && this.playerOption == "scissor") {
      return (this.winner = this.playerName);
    } else if (this.botOption == "paper" && this.playerOption == "rock") {
      return (this.winner = this.botName);
    } else if (this.botOption == "scissor" && this.playerOption == "paper") {
      return (this.winner = this.botName);
    } else if (this.botOption == "scissor" && this.playerOption == "rock") {
      return (this.winner = this.playerName);
    } else if (this.botOption == "rock" && this.playerOption == "paper") {
      return (this.winner = this.playerName);
    } else if (this.botOption == "rock" && this.playerOption == "scissor") {
      return (this.winner = this.botName);
    } else {
      return (this.winner = "SERI");
    }
  }

  matchResult() {
    if (this.winner != "SERI") {
      return `${this.winner} MENANG!`;
    } else {
      return `WAAA ${this.winner}, WAH GAK ADA YG MENANG 🤪`;
    }
  }
}

/* DOM Selector */
const versus = document.querySelector(".versus h1");
const resultClass = document.querySelector(".versus div div");
const textResult = document.querySelector(".versus h5");
const compuBox = document.querySelectorAll(".greyBox.compuImage");
const playerBox = document.querySelectorAll(".greyBox.playerImage");

function wait() {
  const startGame = new Date().getTime();
  let i = 0;
  setInterval(function () {
    if (new Date().getTime() - startGame >= 1000) {
      clearInterval;
      return;
    }

    /*greyBox COM */
    compuBox[i++].style.backgroundColor = "#c4c4c4";
    if (i == compuBox.length) i = 0;

    /* Hilangkan kembali class result saat wait () */
    resultClass.classList.remove("result");

    /* Tampilkan kembali tulisan VS saat wait () */
    versus.style.color = "rgb(189,48,46)";
  }, 50);

  setTimeout(function () {
    setInterval(function () {
      if (new Date().getTime() - startGame >= 1200) {
        clearInterval;
        return;
      }
      /* Menyamarkan greyBox */
      compuBox[i++].style.backgroundColor = "#9c835f";
      if (i == compuBox.length) i = 0;
    }, 50);
  }, 50);
}

function pickOption(params) {
  const start = new Start();
  start.setPlayerOption = params;
  console.log("Kamu memilih:", start.getPlayerOption);
  wait();
  start.setBotOption = start.botBrain();
  start.winCalculation();

  console.log("Bot Pilih :", start.botBrain());
  console.log("win :", start.winCalculation());
  console.log("Result :", start.matchResult());

  /* Samarkan seluruh greyBox pada sisi Player */
  for (let i = 0; i < playerBox.length; i++) {
    playerBox[i].style.backgroundColor = "#9c835f";
  }

  console.log("null ?  ", this.winner);

  /* Eventlistener hanya dikerjakan bila kondisi null */
  if (start.matchResult() !== null) {
    /* Berikan greyBox pada pilihan pemain */
    if (start.getPlayerOption == "rock") {
      playerBox[0].style.backgroundColor = "#c4c4c4";
    } else if (start.getPlayerOption == "paper") {
      playerBox[1].style.backgroundColor = "#c4c4c4";
    } else {
      playerBox[2].style.backgroundColor = "#c4c4c4";
    }

    setTimeout(function () {
      /* Samarkan tulisan VS dengan background s */
      versus.style.color = "#9c835f";

      /* Tampilkan class result */
      resultClass.classList.add("result");

      /* Tampilkan hasil (kotak hijau) */
      textResult.innerHTML = start.matchResult();
      console.log(start.matchResult());
      if (start.matchResult() == "DRAW") {
        textResult.style.backgroundColor = "#225c0e";
      } else {
        textResult.style.backgroundColor = "#4c9654";
      }

      /* Berikan greyBox pada compu choice */
      if (start.getBotOption == "rock") {
        compuBox[0].style.backgroundColor = "#c4c4c4";
      } else if (start.getBotOption == "paper") {
        compuBox[1].style.backgroundColor = "#c4c4c4";
      } else {
        compuBox[2].style.backgroundColor = "#c4c4c4";
      }
    }, 1200);
  } else {
    alert("Click Refresh Button");
  }
}

// TOMBOL RESET
const reset = document.querySelector(".refresh");
reset.addEventListener("click", function () {
  textResult.innerHTML = "";
  resultClass.classList.remove("result");
  for (let i = 0; i < compuBox.length; i++) {
    playerBox[i].style.backgroundColor = "#9c835f";
    compuBox[i].style.backgroundColor = "#9c835f";
  }
  versus.style.color = "rgb(189,48,46)";
  result = null;
});
